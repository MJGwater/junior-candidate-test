import React from 'react';
import Axios from 'axios';

const Entry = (props) => {
  return (
    <tr className={props.entry.year % 2 == 0 ? 'evenColor' : 'oddColor'} onClick={props.handleClick}>
      <td>{props.entry.year}</td>
      <td>{props.entry.actress}</td>
      <td>{props.entry.movie}</td>
    </tr>
  )
}

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      loading: true,
      data: []
    }
  }

  handleClick(year, actress, movie) {
    return () => {
      alert(`year is: ${year} actress is: ${actress} movie is: ${movie}`);
    }
  }

  componentDidMount() {
    const theData = Axios.get('http://127.0.0.1:8000/academy_award_actresses.json')
    theData.then((data) => {
      this.setState({
        loading: false,
        data: data.data
      })
    })
  }

  render() {
    if (this.state.loading) {
      return <p> Loading </p>
    }
    return (
      <table>
        <thead>
          <tr>
            <th>Year</th>
            <th>Actress</th>
            <th>Movie</th>
          </tr>
          {this.state.data.map( (entry) => {
            return <Entry key={entry.year} entry={entry} handleClick={this.handleClick(entry.year, entry.actress, entry.movie)}/>
          })}
        </thead>
      </table>
    );
  }
}

export default App;