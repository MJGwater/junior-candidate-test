const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
  entry: './client/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_bundle.js'
  },
  module: {
    rules: [
      { test: /\.(js)$/, use: 'babel-loader' },
      { test: /\.css$/, use: ['style-loader', 'css-loader']} 
    ]
  },
  devServer: {},
  plugins: [
    new HtmlWebpackPlugin({
      template: 'client/index.html'
    })
  ]
};

module.exports = config;